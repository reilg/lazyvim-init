return {
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        yamlls = {
          settings = {
            yaml = {
              keyOrdering = false,
              customTags = { "!Ref", "!Sub", "!GetAtt" },
            },
          },
        },
      },
    },
  },
}
