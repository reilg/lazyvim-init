return {

  {
    "nvim-telescope/telescope.nvim",
    opts = {
      defaults = {
        mappings = {
          i = {
            -- Use C-jk to move up and down selection
            ["<C-k>"] = "move_selection_previous",
            ["<C-j>"] = "move_selection_next",
          },
        },
      },
      pickers = {
        find_files = {
          theme = "ivy",
        },
      },
    },
  },

  { --Add fzf native
    "telescope.nvim",
    dependencies = {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
      config = function()
        require("telescope").load_extension("fzf")
      end,
    },
  },

  {
    "hrsh7th/nvim-cmp",
    opts = function(_, opts)
      local cmp = require("cmp")
      local addtl_sources = {
        { name = "codeium" },
      }
      opts.sources = cmp.config.sources(vim.list_extend(opts.sources, addtl_sources))
    end,
  },

  { "junegunn/vim-easy-align" },

  {
    -- Markdown preview
    "iamcco/markdown-preview.nvim",
    ft = "markdown",
    build = ":call mkdp#util#install()",
  },
}
