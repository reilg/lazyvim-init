return {
  "Exafunction/codeium.nvim",
  enabled = false,
  dependencies = {
    "nvim-lua/plenary.nvim",
    "hrsh7th/nvim-cmp",
  },
  config = function()
    require("codeium").setup({})
  end,
}

-- return {
--   "Exafunction/codeium.nvim",
--   config = function()
--     -- Change '<C-g>' here to any keycode you like.
--     vim.keymap.set("i", "<tab>", function()
--     return vim.fn["codeium#Accept"]()
--   end, { expr = true })
--
--   vim.keymap.set("i", "<c-n>", function()
--     return vim.fn["codeium#CycleCompletions"](1)
--   end, { expr = true })
--
--   vim.keymap.set("i", "<c-p>", function()
--     return vim.fn["codeium#CycleCompletions"](-1)
--   end, { expr = true })
--
--   vim.keymap.set("i", "<c-x>", function()
--     return vim.fn["codeium#Clear"]()
--   end, { expr = true })
-- end,
-- }
