-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- Tab mapping
vim.keymap.set("n", "<tab>", "<cmd>tabnext<cr>", { desc = "Go to next tab" })
vim.keymap.set("n", "<s-tab>", "<cmd>tabprev<cr>", { desc = "Go to previous tab" })
vim.keymap.set("n", "<leader>n", "<cmd>tabnew<cr>", { desc = "Open new tab" })
vim.keymap.set("n", "<leader><tab>", "<cmd>tabclose<cr>", { desc = "Close current tab" })

-- EasyAlign
vim.keymap.set({ "x", "n" }, "ga", "<Plug>(EasyAlign)", { desc = "Simple alignment plugin" })

-- Markdown Preview
vim.keymap.set("n", "<leader>md", "<Plug>MarkdownPreviewToggle<cr>", { desc = "Markdown preview" })
